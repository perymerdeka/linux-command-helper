echo "downloading package using wget..."
cd /tmp
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
echo "cheking internet connections...."
if ping -q -c 1 -W 1 google.com >/dev/null; then
  echo "Internet is Fast"
else
  echo "The network is down"
fi


echo "installing SDK for dotnet"
sudo apt-get update; \
sudo apt-get install -y apt-transport-https && \
sudo apt-get update && \
sudo apt-get install -y dotnet-sdk-3.1

echo "installing ditnet runtime"
sudo apt-get update; \
sudo apt-get install -y apt-transport-https && \
sudo apt-get update && \
sudo apt-get install -y aspnetcore-runtime-3.1

echo "instalation checking..."
sudo apt-get install -f
echo "installation complete"
